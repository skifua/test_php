<?php
/**
 * Created by PhpStorm.
 * User: kuzin
 * Date: 03.07.15
 * Time: 10:30
 */

class MakePoint extends Make
{

    public function makeNewFigure()
    {
        $point = new Point($this->_firstParameter, $this->_secondParameter);
        $this->commitFigure = 'PointDraw';
        return $point->point();

    }
}