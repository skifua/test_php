<?php
/**
 * Created by PhpStorm.
 * User: kuzin
 * Date: 02.07.15
 * Time: 12:32
 */

abstract class Make
{
    public $commitFigure;
    public $imageData;
    protected $_firstParameter;
    protected $_secondParameter;
    protected $_thirdParameter;

    public function __construct($arrayData)
    {
        if (isset($arrayData['First'])) $this->_firstParameter = $arrayData['First'];
        if (isset($arrayData['Second'])) $this->_secondParameter= $arrayData['Second'];
        if (isset($arrayData['Third'])) $this->_thirdParameter = $arrayData['Third'];

        $this->imageData = $this->makeNewFigure();

    }

    abstract function makeNewFigure();
}
