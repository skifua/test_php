<?php
/**
 * Created by PhpStorm.
 * User: kuzin
 * Date: 03.07.15
 * Time: 14:02
 */

class MakeLineSection extends Make
{
    public function makeNewFigure()
    {
        $point = new LineSection($this->_firstParameter, $this->_secondParameter);
        $this->commitFigure = 'SectionDraw';
        return $point->getLineSection();
    }

}

