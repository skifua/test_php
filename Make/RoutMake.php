<?php
/**
 * Created by PhpStorm.
 * User: kuzin
 * Date: 02.07.15
 * Time: 12:34
 */

class RoutMake
{
    public  $allDataNewFigure;
    public function __construct($arrayData)
    {
        try {
            foreach ($arrayData as $key => $value)
            {
                $nameFigure = strval($key);
                $dataFigure = $value;
            }
            $baseClass = 'Make';
            $targetClass = $baseClass . ucfirst($nameFigure);
            //var_dump($targetClass);

            if (class_exists($targetClass) && is_subclass_of($targetClass, $baseClass))
            {
                $newFigureRout = new $targetClass($dataFigure);
                    $this->allDataNewFigure = array($newFigureRout->commitFigure => $newFigureRout->imageData);
            } else {
                throw new Exception("Unknown figure!");
            }
        } catch (Exception $e) {echo $e;}
    }
}