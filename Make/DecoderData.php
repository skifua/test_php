<?php
/**
 * Created by PhpStorm.
 * User: kuzin
 * Date: 02.07.15
 * Time: 9:03
 * This class get array description figure
 * and return array with key = name identification figure
 */

class DecoderData
{
    private $_dataDescriptionFigure;

    public $dataDecoder;

    public function __construct($array)
    {
        try{
            if (!is_array($array))
            {
                throw new Exception("Error data description geometric figure");
            } else {
                $this->_dataDescriptionFigure = $array;
            }
        } catch (Exception $e) {echo $e;}

        $this->decoderArray();

    }

    public function decoderArray()
    {
        try{
            $dataArray = $this->_dataDescriptionFigure;
            $count = count($this->_dataDescriptionFigure);
            switch ($count){
                case 2:
                {
                    if (array_key_exists('StartPoint', $dataArray) && array_key_exists('EndPoint', $dataArray))
                    {
                        $this->dataDecoder = array('LineSection' => array('First'  => $dataArray['StartPoint'],
                                                                          'Second' => $dataArray['EndPoint']));
                    } elseif(array_key_exists('StartPoint', $dataArray) && array_key_exists('Radius', $dataArray))
                        {
                            $this->dataDecoder = array('Circle' => array('First'  => $dataArray['StartPoint'],
                                                                         'Second' => $dataArray['Radius']));
                    } elseif(array_key_exists('horizontal', $dataArray) && array_key_exists('vertical', $dataArray))
                        {
                            $this->dataDecoder = array('Point' => array( 'First'  => $dataArray['horizontal'],
                                                                         'Second' => $dataArray['vertical']));
                        } else {
                        throw new Exception('Unknown data array');
                    }
                    // etc ...
                    break;
                }

                case 3:
                {
                    if (array_key_exists('A', $dataArray) &&
                        array_key_exists('B', $dataArray) &&
                        array_key_exists('C', $dataArray))
                    {

                        $this->dataDecoder = array('Triangle' => array( 'First'  => $dataArray['A'],
                                                                        'Second' => $dataArray['B'],
                                                                        'Third'  => $dataArray['C']));
                    } elseif (array_key_exists('SideA', $dataArray) &&
                             array_key_exists('Direct', $dataArray) &&
                             array_key_exists('SizeB', $dataArray))
                    { $this->dataDecoder = array('Rectangle' => $dataArray);
                        $this->dataDecoder = array('Rectangle' => array( 'First'  => $dataArray['SideA'],
                                                                         'Second' => $dataArray['Direct'],
                                                                         'Third'  => $dataArray['SizeB']));
                    } else {
                        throw new Exception('Unknown data array');
                    }
                    //etc ...
                    break;
                }
                default:
                    throw new Exception('Unknown data array');
                    break;

            }
        } catch (Exception $e) {echo $e;}
    }


}