<?php
/**
 * description square
 * Created by PhpStorm.
 * User: kuzin
 * Date: 17.06.15
 * Time: 15:54
 */

class Square
{
    private $_side;
    private $_direct;

    function __construct(LineSection $side,$direction)
    {
        $this->_side = $side;
        $this->_direct = VerificationAttribute::stringData($direction);
    }

    public function square()
    {
        return array('SideA' => $this->_side, 'Direct' => $this->_direct);
    }

}