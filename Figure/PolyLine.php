<?php
/**
 * description curve
 * recuare
 * Created by PhpStorm.
 * User: kuzin
 * Date: 17.06.15
 * Time: 15:28
 */

class PolyLine
{
    private $_polyLine = array();

    function __construct($arrayPoints) //add array with object
    {
        try {
            if (!is_array($arrayPoints)) throw new Exception("error verification array data");

                foreach($arrayPoints as $key => $value)
                {
                    if (!method_exists($value,'point') || get_class($value) != 'Point')
                        throw new Exception("verification point - error object");

                    $this->_polyLine[] = $value;
                }
            } catch (Exception $e) {
                echo "ERROR: " . $e;
        }
    }

    public function polyLine()
    {
        return $this->_polyLine;
    }
}