<?php
/**
 * description square
 * Created by PhpStorm.
 * User: kuzin
 * Date: 17.06.15
 * Time: 15:07
 */

class Rectangle
{
    private $_sideA;
    private $_directB;
    private $_size;

    function __construct(LineSection $side,$direction,$size)
    {
        $this->_sideA = $side;
        $this->_directB = VerificationAttribute::stringData($direction);
        $this->_size = VerificationAttribute::numeric($size);
    }

    public function rectangle()
    {
        return array('SideA' => $this->_sideA, 'Direct' => $this->_directB, 'SizeB' => $this->_size);
    }
}