<?php
/**
 * description section
 * Created by PhpStorm.
 * User: kuzin
 * Date: 16.06.15
 * Time: 14:23
 */

class LineSection
{
    private $_startPoint;
    private $_endPoint;

    function __construct($startPoint,$endPoint)
    {
        $this->setStartPoint($startPoint);
        $this->setEndPoint($endPoint);
    }

    public function getStartPoint()
    {
        return $this->_startPoint;
    }

    public function getEndPoint()
    {
        return $this->_endPoint;
    }

    public function setStartPoint(Point $startPoint)
    {
        $this->_startPoint = $startPoint;
    }

    public function setEndPoint(Point $endPoint)
    {
        $this->_endPoint = $endPoint;
    }

    public function getLineSection()
    {
        return array('StartPoint' => $this->getStartPoint(), 'EndPoint' => $this->getEndPoint());
    }
}