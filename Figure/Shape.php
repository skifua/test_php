<?php
/**
 * basic class
 * Created by PhpStorm.
 * User: kuzin
 * Date: 16.06.15
 * Time: 13:20
 */

class Shape
{
    private $_horisontal;
    private $_vertical;

    protected function getCoordinates($horisontal,$vertical)
    {
        //VerificationAttribute::numeric($horisontal);
        $this->_horisontal = VerificationAttribute::numeric($horisontal);
        $this->_vertical = VerificationAttribute::numeric($vertical);;
    }

    protected function dot()
    {
        return array('horisontal' => $this->_horisontal, 'vertical' => $this->_vertical);
    }

}