<?php
/**
 * point to dot
 * Created by PhpStorm.
 * User: kuzin
 * Date: 16.06.15
 * Time: 13:49
 */

class Point
{
    private $_horizontal;
    private $_vertical;

    function __construct($horizontal,$vertical)
    {
        $this->_horizontal = VerificationAttribute::numeric($horizontal);
        $this->_vertical = VerificationAttribute::numeric($vertical);;
    }

    public function point()
    {
        return array('horizontal' => $this->_horizontal, 'vertical' => $this->_vertical);
    }
}