<?php
/**
 * Created by PhpStorm.
 * User: kuzin
 * Date: 19.06.15
 * Time: 9:36
 */

class VerificationAttribute
{

    public static function numeric($var)
    {
       try {
           if (!is_int($var)) throw new Exception("error verification integer");
               return $var;
           } catch (Exception $e) {
           echo "ERROR: " . $e;
       }
    }

    public static function stringData($var)
    {

        try { if (!is_string($var)) throw new Exception("error verification string");
                return trim($var);
            } catch (Exception $e) {
            echo "ERROR: " . $e;
        }
    }

    public static function floatNumber($var)
    {
        try { if (!is_float($var)) throw new Exception("error verification float data");
            return $var;
        } catch (Exception $e) {
            echo "ERROR: " . $e;
        }
    }

    public static function arrayPoint($var)
    {
        try { if (!is_array($var)) throw new Exception("error verification array data");
            return $var;
        } catch (Exception $e) {
            echo "ERROR: " . $e;
        }
    }



}