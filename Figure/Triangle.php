<?php
/**
 * description triangle
 * Created by PhpStorm.
 * User: kuzin
 * Date: 17.06.15
 * Time: 14:11
 */

class Triangle
{
    private $_dotA;
    private $_dotB;
    private $_dotC;

    function __construct(Point $dotA, Point $dotB, Point $dotC)
    {
        $this->_dotA = $dotA;
        $this->_dotB = $dotA;
        $this->_dotC = $dotA;
    }

    public function triangle()
    {
        return array('A' => $this->_dotA, 'B' => $this->_dotB, 'C' => $this->_dotC);
    }
}