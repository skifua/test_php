<?php
/**
 * description circle
 * Created by PhpStorm.
 * User: kuzin
 * Date: 17.06.15
 * Time: 13:12
 */

class Circle
{
    private $_centerDot;
    private $_radiusCircle;

    function __construct(Point $centerDot, $radius)
    {
        $this->_centerDot = $centerDot;
        $this->_radiusCircle = VerificationAttribute::numeric($radius);
    }

    public function circle()
    {
        return array('Center' => $this->_centerDot, 'Radius' => $this->_radiusCircle);
    }
}