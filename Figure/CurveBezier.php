<?php
/**
 * description curve Besier
 * Created by PhpStorm.
 * User: kuzin
 * Date: 17.06.15
 * Time: 18:44
 */

class CurveBezier extends PolyLine
{
    private $_bazisFunction;
    private $_polyLine;

    function __construct($polyLine,$bazisFunction)
    {
        parent::__construct($polyLine);
        $this->_polyLine = $this->polyLine();
        try {
            if (VerificationAttribute::floatNumber($bazisFunction) >  1) throw new Exception('error varification bazis function');
            $this->_bazisFunction = $bazisFunction;
        } catch (Exception $e){
            echo 'ERROR' . $e;
        }

    }

    public function curveBezier()
    {
        return array('BazisFunction' => $this->_bazisFunction, 'PivotPoints' => $this->_polyLine);
    }

}